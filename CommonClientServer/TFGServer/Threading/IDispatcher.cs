﻿using System;

namespace TFGCommonLib.Threading {
    interface IDispatcher {
        void Invoke(Action fn);
    }
}