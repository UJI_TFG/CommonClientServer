﻿using System;
using System.Collections.Generic;

namespace TFGCommonLib.Threading {
    public class Dispatcher : IDispatcher {

        private static Dispatcher instance;
        public static Dispatcher Instance {
            get {
                if (instance == null) {
                    // Instance singleton on first use.
                    instance = new Dispatcher();
                }
                return instance;
            }
        }

        public List<Action> pending = new List<Action>();

        /// <summary>
        /// Schedule code for execution in the main-thread.
        /// </summary>
        /// <param name="fn">Action</param>
        public void Invoke(Action fn) {
            lock (pending) {
                pending.Add(fn);
            }
        }

        /// <summary>
        /// Execute pending actions.
        /// </summary>
        public void InvokePending() {
            lock (pending) {
                foreach (var action in pending) {
                    action(); // Invoke the action.
                }

                pending.Clear(); // Clear the pending list.
            }
        }
    }
}