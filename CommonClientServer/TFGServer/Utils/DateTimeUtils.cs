﻿using System;

namespace TFGCommonLib.Utils {
    public static class DateTimeUtils {
        public static DateTime TimestampToDateTime(double unixTimeStamp) {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            Epoch = Epoch.AddSeconds(unixTimeStamp).ToLocalTime();
            return Epoch;
        }

        public static long DateTimeToTimestamp(DateTime value) {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }
    }
}
