﻿using System;
using TFGCommonLib.Utils;

namespace TFGCommonLib.Networking {
    public static class ErrorMessageHandler {

        public static string ErrorMessage(string input) {

            string[] tokens = input.Split(' ');

            if (tokens[1] != null) {
                switch (int.Parse(tokens[1])) {
                    case (int)ErrorCode.SERVER_UNVAILABLE:
                        return "Server not available. The server may be under maintenance or your internet is experiencing some connection issues.";
                    case (int)ErrorCode.LOGIN_TIMEOUT:
                        return "Time out. The server not responded to your login query. Please try a again.";
                    case (int)ErrorCode.USER_EMPTY:
                        return "The username field is empty.";
                    case (int)ErrorCode.PASS_EMPTY:
                        return "The password field is empty.";
                    case (int)ErrorCode.USER_PASS_EMPTY:
                        return "The username and password fields are empty.";
                    case (int)ErrorCode.WRONG_LOGIN:
                        return "The username and password combination you introduced is not registered.";
                    case (int)ErrorCode.SERVER_ERROR:
                        return "Unknown error. The server reported an error during the requested operation. Please try again.";
                    case (int)ErrorCode.BANNED_USER:
                        if (tokens[2] != null)
                            return "The user you are trying to log in is banned until: " + DateTimeUtils.TimestampToDateTime(Double.Parse(tokens[2])) + " UTC.";
                        return "The user you are trying to log in is banned.";
                }
            }

            return null;
        }

    }
}
