﻿namespace TFGCommonLib.Networking {
    public enum OpCode {
        NULL,
        LOGIN,
        LOGIN_OK,
        DISCONNECT,
        ERROR,
        GET_VERSION,
        VERSION,
        REGISTER_USER,
        DOWNLOAD_CLIENT,
        TRANSFER_INFO,
        TRANSFER_PREPARED,
        TRANSFER_FINISHED,
        SEND_FILE,
        UPDATE_USER,
        CREATE_MATCH,
        MATCH_ID,
        UPDATE_MATCH,
        ADD_MATCH_PLAYER,
        UPDATE_MATCH_PLAYER,
        REQUEST_EXP_RATE,
        EXP_RATE
    };
}
