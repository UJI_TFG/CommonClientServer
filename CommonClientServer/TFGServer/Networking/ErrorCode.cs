﻿namespace TFGCommonLib.Networking {
    public enum ErrorCode {
        SERVER_UNVAILABLE = -1,
        SERVER_ERROR = 0,
        USER_PASS_EMPTY = 1,
        USER_EMPTY = 2,
        PASS_EMPTY = 3,
        WRONG_LOGIN = 4,
        BANNED_USER = 5,
        TIME_OUT = 6,
        LOGIN_TIMEOUT = 7
    };
}
