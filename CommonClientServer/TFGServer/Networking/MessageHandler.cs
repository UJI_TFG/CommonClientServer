﻿using System;

namespace TFGCommonLib.Networking {
    public static class MessageHandler {
        public static string CreateMessage(OpCode opCode, params string[] args) {
            switch (opCode) {
                default:
                    return "";
                case OpCode.LOGIN:
                    if (args.Length == 2)
                        return "LOGIN " + args[0] + " " + args[1] + " \n";
                    throw new Exception("ERROR: OpCode \"LOGIN\" needs two arguments (user, shaPass).");
                case OpCode.LOGIN_OK:
                    if (args.Length == 2)
                        return "LOGIN_OK " + args[0] + " " + args[1] + " \n";
                    throw new Exception("ERROR: OpCode \"LOGIN_OK\" needs two arguments (userID, exp).");
                case OpCode.DISCONNECT:
                    if (args.Length == 0)
                        return "DISCONNECT" + " \n";
                    throw new Exception("ERROR: OpCode \"DISCONNECT\" don't need any argument.");
                case OpCode.GET_VERSION:
                    if (args.Length == 0)
                        return "GET_VERSION" + " \n";
                    throw new Exception("ERROR: OpCode \"GET_VERSION\" don't need any argument.");
                case OpCode.VERSION:
                    if (args.Length == 3)
                        return "VERSION " + args[0] + " " + args[1] + " " + args[2] + " \n";
                    throw new Exception("ERROR: OpCode \"VERSION\" needs three arguments (major, minor, revision).");
                case OpCode.DOWNLOAD_CLIENT:
                    if (args.Length == 0)
                        return "DOWNLOAD_CLIENT" + " \n";
                    throw new Exception("ERROR: OpCode \"DOWNLOAD_CLIENT\" don't need any argument.");
                case OpCode.TRANSFER_INFO:
                    if (args.Length == 1)
                        return "TRANSFER_INFO " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"TRANSFER_INFO\" needs one argument (numberFiles).");
                case OpCode.TRANSFER_PREPARED:
                    if (args.Length == 0)
                        return "TRANSFER_PREPARED" + " \n";
                    throw new Exception("ERROR: OpCode \"TRANSFER_PREPARED\" don't need any argument.");
                case OpCode.TRANSFER_FINISHED:
                    if (args.Length == 0)
                        return "TRANSFER_FINISHED" + " \n";
                    throw new Exception("ERROR: OpCode \"TRANSFER_FINISHED\" don't need any argument.");
                case OpCode.SEND_FILE:
                    if (args.Length == 2)
                        return "SEND_FILE " + args[0] + " " + args[1] + " \n";
                    throw new Exception("ERROR: OpCode \"SEND_FILE\" needs two arguments (filePath, fileSize).");
                case OpCode.UPDATE_USER:
                    if (args.Length == 1)
                        return "UPDATE_USER " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"UPDATE_USER\" needs one argument (exp).");
                case OpCode.CREATE_MATCH:
                    if (args.Length == 1)
                        return "CREATE_MATCH " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"CREATE_MATCH\" needs one argument (gamemodeID).");
                case OpCode.MATCH_ID:
                    if (args.Length == 1)
                        return "MATCH_ID " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"MATCH_ID\" needs two arguments (macthID).");
                case OpCode.UPDATE_MATCH:
                    if (args.Length == 3)
                        return "UPDATE_MATCH " + args[0] + " " + args[1] + " " + args[2] + " \n";
                    throw new Exception("ERROR: OpCode \"UPDATE_MATCH\" needs three arguments (matchID, endDate, winnerID).");
                case OpCode.ADD_MATCH_PLAYER:
                    if (args.Length == 1)
                        return "ADD_MATCH_PLAYER " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"ADD_MATCH_PLAYER\" needs one argument (matchID).");
                case OpCode.UPDATE_MATCH_PLAYER:
                    if (args.Length == 4)
                        return "UPDATE_MATCH_PLAYER " + args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " \n";
                    throw new Exception("ERROR: OpCode \"UPDATE_MATCH_PLAYER\" needs four arguments (matchID, personalScore, eliminations, deaths).");
                case OpCode.REQUEST_EXP_RATE:
                    if (args.Length == 0)
                        return "REQUEST_EXP_RATE" + " \n";
                    throw new Exception("ERROR: OpCode \"REQUEST_EXP_RATE\" don't need any argument.");
                case OpCode.EXP_RATE:
                    if (args.Length == 1)
                        return "EXP_RATE " + args[0] + " \n";
                    throw new Exception("ERROR: OpCode \"EXP_RATE\" needs one argument (expRate).");
            }
        }

        public static string CreateMessage(ErrorCode errorCode, params string[] args) {
            switch (errorCode) {
                default:
                    if (args.Length == 0)
                        return "ERROR " + ((int)errorCode).ToString();
                    throw new Exception("ERROR: OpCode \"ERROR\" needs one argument (errorCode).");
                case ErrorCode.BANNED_USER:
                    if (args.Length == 1)
                        return "ERROR " + ((int)errorCode).ToString() + " " + args[0];
                    throw new Exception("ERROR: OpCode \"ERROR:BANNED_USER\" needs two arguments (errorCode, until_date).");
            }
        }

        public static OpCode ReadOpCode(string message) {
            string[] tokens = message.Split(' ');
            if (tokens.Length > 0) {
                return (OpCode)Enum.Parse(typeof(OpCode), tokens[0]);
            }
            return OpCode.NULL;
        }

        public static string[] ReadParams(string message) {
            string[] tokens = message.Split(' ');
            if (tokens.Length > 0) {
                string[] args = new string[tokens.Length - 1];
                Array.Copy(tokens, 1, args, 0, tokens.Length - 1);
                return args;
            }
            return null;
        }
    }

}
