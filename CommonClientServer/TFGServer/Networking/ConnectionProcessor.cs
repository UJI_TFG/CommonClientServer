﻿namespace TFGCommonLib.Networking {
    public abstract class ConnectionProcessor {
        public abstract void Send(ErrorCode errorCode, params string[] args);
        public abstract void Send(OpCode opCode, params string[] args);
        protected abstract void send(string message);
        protected abstract void receive();
        public abstract string Receive();
    }
}
